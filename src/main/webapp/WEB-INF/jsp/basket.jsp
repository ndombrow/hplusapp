<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="display" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<link rel="stylesheet" href="css/style.css">
<meta charset="ISO-8859-1">
<title>Hplus</title>
</head>
<body>
	<header id="home" class="header">
		<nav class="nav" role="navigation">
			<div class="container nav-elements">
				<div class="branding">
					<a href="#home"><img src="images/hpluslogo.svg"
						alt="Logo - H Plus Sports"></a>
				</div>
				<!-- branding -->
				<ul class="navbar">
					<li><a href="/home">home</a></li>
					<li><a href="/goToSearch">search</a></li>
					<li><a href="/redirectionToLinkedin">linkedIn</a></li>
				</ul>
				<!-- navbar -->
			</div>
			<!-- container nav-elements -->
		</nav>
		<!-- <div class="container tagline">
    <h1 class="headline">Our Mission</h1>
    <p>We support and encourage <em>active and healthy</em> lifestyles, by offering <em>ethically sourced</em> and <em>eco-friendly</em> nutritional products for the <em>performance-driven</em> athlete.</p>
  </div>container tagline -->
	</header>

	<c:if test ="${!empty(productsList.products)}">
		<section id="products" class="section">
			<form:form action="/pay"  method="post" modelAttribute = "productsList">
				<div class="productListContainer">
					<table class="supercheckout-summary">
						<thead>
							<th class="supercheckout-name">Description</th>
							<th class="supercheckout-amount">Amount</th>
							<th class="supercheckout-total">Total Price</th>
						</thead>
						<tbody>
							<c:forEach var="product" items="${productsList.products}" varStatus="status">
								<tr>
									<td>
										<img id="pic2" src="${product.imagePath}">
										<input name="products[${status.index}].name" type="text" value="${product.name}"/>
									</td>
									<td>
										<input name="products[${status.index}].amount" type="text" value="${product.amount}"/>
									</td>
									<td>
										<input name="products[${status.index}].price" type="text" value="${product.price}"/>
									</td>
									<td>
										<a href><p>x</p></a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

				<input type="submit" value="Pay">

			</form:form>
		</section>
	</c:if>
	<!-- #home -->


	<footer class="footer">
		<div class="container">

			<nav class="nav" role="navigation">
				<div class="container nav-elements">
					<div class="branding">
						<a href="#home"><img src="images/hpluslogo.svg"
							alt="Logo - H Plus Sports"></a>
						<p class="address">
							100 Main Street<br> Seattle, WA 98144
						</p>
					</div>
				</div>
			</nav>
			<p class="legal">H+ Sport is a fictitious brand created by
				lynda.com solely for the purpose of training. All products and
				people associated with H+ Sport are also fictitious. Any resemblance
				to real brands, products, or people is purely coincidental.
				Information provided about the product is also fictitious and should
				not be construed to be representative of actual products on the
				market in a similar product category.</p>
		</div>
		<!-- container -->
	</footer>
	<!-- footer -->

</body>
</html>