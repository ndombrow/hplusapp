<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
<meta charset="UTF-8">
<title>H+ Sport</title>
<link rel="stylesheet" href="css/style.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

	<header id="home" class="header">
		<nav class="nav" role="navigation">
			<div class="container nav-elements">
				<div class="branding">
					<a href="home"><img src="images/hpluslogo.svg"
						alt="Logo - H Plus Sports"></a>
				</div>
				<!-- branding -->
				<ul class="navbar">
                                <li><a href="/home">home</a></li>
                                <li><a href="/goToLogin">login</a></li>
								<li><a href="/goToBasket">basket</a></li>
                                <li><a href="/redirectionToLinkedin">linkedin</a></li>
                            </ul><!-- navbar -->
				<!-- navbar -->
			</div>
			<!-- container nav-elements -->
		</nav>
	</header>
	<!-- #home -->

	<section id="search" class="section">
		<header class="imageheader"></header>
		<div class="container">
			<h2 class="headline">Search Products</h2>
			<form action="/search" method="get">
				<label class="card-title">Search your product</label>
				 <input path="search" name="search" value="">
			    <input type="submit" value="Search">
			</form>
		</div>
	</section>
	<!-- guarantee -->

    <c:if test ="${!empty(productsList.products)}">
		<section id="products" class="section">
			<form:form action="/selectProducts"  method="post" modelAttribute = "productsList">
				<c:forEach var="product" items="${productsList.products}" varStatus="status">
					<div class="productContainerItem">
						<input name="products[${status.index}].id" type="hidden" value="${product.id}"/>
						<input name="products[${status.index}].imagePath" type="hidden" value="${product.imagePath}"/>

						<img id="pic1" src="${product.imagePath}">
						<input name="products[${status.index}].name" type="text" value="${product.name}"/>
						<input name="products[${status.index}].price" type="text" value="${product.price}"/>
						<div class="dropdown-select-amount">
							<select class="dropdown-select-amount" name="products[${status.index}].amount">
								<option selected value="0" path="amount">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
						</div>
					</div>
				</c:forEach>
				<input type="submit" value="Go to basket">
			</form:form>
		</section>
	</c:if>


		<section id="history" class="section">
		<div class="container">
			<div class="text-content">
				<h2 class="headline">Company History</h2>
				<p>
					In 2006, H+ Sport founder <a href="#employees-henrytwill">Henry
						Twill</a> set out to create supplements and nutritional products made
					from the <em>purest ingredients</em> and backed by scientific and <em>performance
						data</em> – to provide a strong basis of <em>trust and
						accountability</em>.
				</p>
				<p>
					His quest began while training for his <em>first 5K marathon</em>,
					after recovering from a <em>lengthy injury</em> that left him with
					residual aches, pains, and decreased athletic performance. He
					yearned for <em>simple and natural ingredients</em>, in their
					purest form, instead of labels with <em>lengthy chemical names</em>
					and disclaimers. When no products lived up to his strict
					requirements, Henry set his sights on <em>creating products</em>
					that catered to his own vision.
				</p>
				<p>
					Today that vision has become H+ Sport…offering a variety of <a
						href="#products">supplements, energy bars and rehydration
						solutions</a>. At the foundation of H+ Sports are products backed by
					scientific and performance data.
				</p>
			</div>
		</div>
		<!-- container text -->
	</section>


	<footer class="footer">
		<div class="container">
			<nav class="nav" role="navigation">
				<div class="container nav-elements">
					<div class="branding">
						<a href="#home"><img src="images/hpluslogo.svg"
							alt="Logo - H Plus Sports"></a>
						<p class="address">
							100 Main Street<br> Seattle, WA 98144
						</p>
					</div>
				</div>
			</nav>
			<p class="legal">H+ Sport is a fictitious brand created by
				lynda.com solely for the purpose of training. All products and
				people associated with H+ Sport are also fictitious. Any resemblance
				to real brands, products, or people is purely coincidental.
				Information provided about the product is also fictitious and should
				not be construed to be representative of actual products on the
				market in a similar product category.</p>
		</div>
		<!-- container -->
	</footer>
	<!-- footer -->
</body>
</html>