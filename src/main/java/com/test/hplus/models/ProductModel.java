package com.test.hplus.models;

import com.test.hplus.beans.Product;

import javax.persistence.Entity;
import javax.persistence.Id;

public class ProductModel {
    private int id;
    private String name;

    public ProductModel(Product product) {
        id = product.getId();
        name = product.getName();
        price = product.getPrice();
        imagePath = product.getImagePath();
        amount = 0;
    }

    public ProductModel() {

    }

    private String imagePath;

    private double price;

    private int amount;

    public int getAmount() {

        return amount;
    }

    public void setAmount(int amount) {

        this.amount = amount;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getImagePath() {

        return imagePath;
    }

    public void setImagePath(String imagePath) {

        this.imagePath = imagePath;
    }

    public double getPrice() {

        return price;
    }

    public void setPrice(double price) {

        this.price = price;
    }
}
