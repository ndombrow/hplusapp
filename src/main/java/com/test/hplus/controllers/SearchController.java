package com.test.hplus.controllers;

import com.test.hplus.beans.Product;
import com.test.hplus.beans.ProductsList;
import com.test.hplus.models.ProductModel;
import com.test.hplus.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
public class SearchController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/search")
    public String search(@RequestParam("search")String search, Model model){
        System.out.println("in search controller");
        System.out.println("search" + search);

        List<Product> products = new ArrayList<>();
        products = productRepository.searchByName(search);
        ProductsList productsList = new ProductsList();

        List<ProductModel> productModels = new ArrayList<>();
        products.forEach(p -> productModels.add(new ProductModel(p)));
        productsList.setProducts(productModels);

        model.addAttribute("productsList", productsList);
        /*for(Product product:products){
            System.out.println(product.getName());
        } */
        return "search";
    }
    @PostMapping("/selectProducts")
    public String selectProducts(@ModelAttribute("productsList") ProductsList productsList, Model model){

        List<ProductModel> list = productsList.getProducts().stream().filter(p->p.getAmount()!=0).collect(Collectors.toList());

        for(int i=0;i<list.size();i++){
           ProductModel productModel = list.get(i);
           int amount = productModel.getAmount();
           double totalPrice = productModel.getPrice() * amount;
           totalPrice = Math.round(totalPrice*100);
           totalPrice = totalPrice/100;
           productModel.setPrice(totalPrice);
           productModel.setAmount(amount);
        }

        productsList.setProducts(list);
        System.out.println("in select product controller");
        model.addAttribute("productsList", productsList);
        return "basket";
    }
}
