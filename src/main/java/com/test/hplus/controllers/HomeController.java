package com.test.hplus.controllers;

import com.test.hplus.beans.Product;
import com.test.hplus.beans.ProductsList;
import com.test.hplus.models.ProductModel;
import com.test.hplus.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/home")
    public String goHome(){
        System.out.println("in home controller");
        return "index";
    }

    @GetMapping("/goToSearch")
    public String goToSearch(Model model){
        System.out.println("going to search");

        List<Product> products = new ArrayList<>();
        Iterable<Product> iterator = productRepository.findAll();
        iterator.forEach(products::add);
        ProductsList productsList = new ProductsList();

        List<ProductModel> productModels = new ArrayList<>();
        products.forEach(p -> productModels.add(new ProductModel(p)));

        productsList.setProducts(productModels);
        model.addAttribute("productsList", productsList);
        return "search";
    }

    @GetMapping("/goToLogin")
    public String goToLogin(){
        System.out.println("going to login");
        return "login";
    }

    @GetMapping("/goToRegistration")
    public String goToRegistration(){
        System.out.println("going to register");
        return "register";
    }

    @GetMapping("/goToBasket")
    public String goToBasket(){
        System.out.println("going to basket");
        return "basket";
    }

    /*@ModelAttribute("newuser")
    public User getDefaultUser(){

        return new User();
    }

    @ModelAttribute("genderItems")
    public List<String> getGenderItems(){

        return Arrays.asList(new String[]{"Male", "Female", "Other"});
    }

    @ModelAttribute("login")
    public Login getDefaultLogin(){

        return new Login();
    } */
}
