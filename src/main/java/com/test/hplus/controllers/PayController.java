package com.test.hplus.controllers;

import com.test.hplus.beans.Order;
import com.test.hplus.beans.OrderRow;
import com.test.hplus.beans.ProductsList;
import com.test.hplus.beans.User;
import com.test.hplus.models.ProductModel;
import com.test.hplus.repository.OrderRepository;
import com.test.hplus.repository.OrderRowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Controller
public class PayController {

    @Autowired
    private OrderRowRepository orderRowRepository;
    @Autowired
    private OrderRepository orderRepository;

    @PostMapping("/pay")
    public String payWindow (@ModelAttribute("productsList")ProductsList productsList, BindingResult result){

        Date date = new Date();
        Timestamp timestamp = new Timestamp((new Date()).getTime());

        Order myOrder = new Order();
        System.out.println("in pay controller");
        myOrder.setTimestamp(timestamp);
        orderRepository.save(myOrder);

        List<ProductModel> list = productsList.getProducts();
        for(int i=0;i<list.size();i++){
            OrderRow orderRow = new OrderRow();
            ProductModel productModel = list.get(i);
            int id = productModel.getId();
            int amount = productModel.getAmount();
            double totalPrice = productModel.getPrice();
            int orderId=myOrder.getId();
            orderRow.setProduct_id(id);
            orderRow.setAmount(amount);
            orderRow.setTotal_price(totalPrice);
            orderRow.setOrder_id(orderId);
            orderRowRepository.save(orderRow);
        }
        return "pay";

        //orderRowRepository.save(orderRow);

    }

}
