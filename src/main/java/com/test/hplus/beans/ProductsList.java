package com.test.hplus.beans;

import com.test.hplus.models.ProductModel;

import java.util.List;

public class ProductsList {
    private List<ProductModel> products;

    public List<ProductModel> getProducts() {

        return products;
    }

    public void setProducts(List<ProductModel> products) {

        this.products = products;
    }
}
