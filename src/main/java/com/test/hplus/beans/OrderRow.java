package com.test.hplus.beans;

import javax.persistence.*;

@Entity
@Table(name="order_row")
public class OrderRow {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private int product_id;
    private int amount;
    private double total_price;
    private int order_id;

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public int getProduct_id() {

        return product_id;
    }

    public void setProduct_id(int product_id) {

        this.product_id = product_id;
    }

    public int getAmount() {

        return amount;
    }

    public void setAmount(int amount) {

        this.amount = amount;
    }

    public double getTotal_price() {

        return total_price;
    }

    public void setTotal_price(double total_price) {

        this.total_price = total_price;
    }

    public int getOrder_id() {

        return order_id;
    }

    public void setOrder_id(int order_id) {

        this.order_id = order_id;
    }
}
