package com.test.hplus.repository;

import com.test.hplus.beans.OrderRow;
import com.test.hplus.beans.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface OrderRowRepository extends CrudRepository<OrderRow, Integer> {

    /*@Query("select o from OrderRow o where o.product_id=:productId")
    public OrderRow searchById(@Param("productId")int product_id); */
}
