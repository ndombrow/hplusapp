package com.test.hplus.repository;

import com.test.hplus.beans.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer> {
}
